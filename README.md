# coordinates

Permet de récupérer la latitude et longitude d'une adresse dans un CSV.

Ce projet utilise l'api Nominatim OpenStreetMap : [text](https://nominatim.openstreetmap.org)

Documentation : [text](https://nominatim.org/release-docs/latest/api/Search)

Pour installer l'outil :

*Prérequis : installer composer : https://getcomposer.org*

`git clone git@gitlab.com:gundesli/coordinates.git`

`cd coordinates`

`composer update`

L'outil propose un formulaire pour uploader un fichier CSV avec une seule colonne pour l'adresse.

La première ligne correspond à l'entête du fichier CSV, elle ne contient qu'une seule colonne intitulée : adresse.
Ensuite chaque ligne du fichier doit contenir une adresse postale entre les doubles quotes.

Voici un exemple de fichier CSV :

```
adresse
"avenue du Général Leclerc 54000 Nancy"
```


Après avoir uploader le fichier CSV, l'outil fait appel à l'API et génère un nouveau fichier CSV avec 2 colonnes en plus : latitude et longitude

Voici le résultat avec le fichier d'exemple ci-dessus :

```
adresse,latitude,longitude
"avenue du Général Leclerc 54000 Nancy",48.6778758,6.1789589
```