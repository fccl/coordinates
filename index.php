<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Récupérer la latitude et longitude à partir d'une adresse</title>
</head>
<body>
    <form method="post" action="recuperer.php" enctype="multipart/form-data">
        <input type="file" name="csvFile">
        <input type="submit" value="Lancer la récupération" name="submit">
    </form>
</body>
</html>